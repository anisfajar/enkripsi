<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>-- Tools Enkrip Generator --</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="icon" href="<?php echo site_url('asset/img/enkrip.png') ?>" type="png">
</head>
<body>
    <?php $this->load->view('sidebar.php'); ?>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <p><br>

    <form action="<?php echo site_url('hashsha512/enkrip') ?>" method="post">
        <div class="container">
            <div class="form-group">
                <center><h2>HASH SHA512 Encrypt Generator</h2></center>
                <center>* Menggunakan Modifikasi Encryption Key bawaan Codeigniter *<br><p><p></center>
            </div>
            <div class="form-group">
                <label>Masukkan Karakter</label><br>
                <input type="text" autocomplete="off" class="form-control" name="karakter">
                <strong><label>* Config Encryption Key sesuaikan kasus masing - masing </label></strong>
            </div>
            <div class="form-group">
                <input type="submit" name="encrypt" class="btn btn-info" value="Encrypt HASH SHA512">
            </div>
            <br>
        </div>
    </form>

    <footer class="page-footer font-small blue">
        <div class="footer-copyright text-center py-3">© 2019 Copyright:
            Anis Fajar Prakoso</a>
        </div>
    </footer>
</body>
</html>