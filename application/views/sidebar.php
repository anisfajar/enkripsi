<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo site_url('haldep') ?>">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo site_url('md5') ?>">Enkrip MD5 <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo site_url('base64encode') ?>">Base64 Encode <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo site_url('base64decode') ?>">Base64 Decode <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo site_url('sha1') ?>">Enkrip SHA1 <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo site_url('crc32') ?>">Enkrip CRC32 <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active"> 
                <a class="nav-link" href="<?php echo site_url('hashsha512') ?>">Hash SHA512 <span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
</nav>