<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Md5 extends CI_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $this->load->view('md5/md5input');
    }
    
    public function enkrip(){
        $karakter = $this->input->post('karakter');
        $generate = md5($karakter);

        if ($this->input->post('encrypt')) {     
            $data = array(
                'hasil' => $generate
            );
            $this->load->view('md5/md5hasil',$data);
        } else {
            $this->load->view('md5/md5input');
        }
    }
}