<?php

class Hashsha512 extends CI_Controller
{
    public function __construct(){
        parent::__construct();
    }
    
    public function index(){
        $this->load->view('hashsha512/hashsha512input');
    }

    public function enkrip(){
        $karakter = $this->input->post('karakter');
        $generate = hash('sha512', $karakter . config_item('encryption_key'));
        if ($this->input->post('encrypt')) {     
            $data = array(
                'hasil' => $generate
            );
            $this->load->view('hashsha512/hashsha512hasil',$data);
        } else {
            $this->load->view('hashsha512/hashsha512input');
        }
    }
}
