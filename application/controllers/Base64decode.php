<?php 

class Base64decode extends CI_Controller
{
    public function __construct(){
        parent::__construct();
    }
    
    public function index(){
        $this->load->view('base64decode/base64decodeinput');
    }

    public function enkrip(){
        $karakter = $this->input->post('karakter');
        $generate = base64_decode($karakter);

        if ($this->input->post('encrypt')) {     
            $data = array(
                'hasil' => $generate
            );
            $this->load->view('base64decode/base64decodehasil',$data);
        } else {
            $this->load->view('base64decode/base64decodeinput');
        }
    }
}
