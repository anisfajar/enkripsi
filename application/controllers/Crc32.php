<?php 

class Crc32 extends CI_Controller
{
    public function __construct(){
        parent::__construct();
    }
    
    public function index(){
        $this->load->view('crc32/crc32input');
    }

    public function enkrip(){
        $karakter = $this->input->post('karakter');
        $generate = crc32($karakter);

        if ($this->input->post('encrypt')) {     
            $data = array(
                'hasil' => $generate
            );
            $this->load->view('crc32/crc32hasil',$data);
        } else {
            $this->load->view('crc32/crc32input');
        }
    }
}
