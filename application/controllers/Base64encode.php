<?php 

class Base64encode extends CI_Controller
{
    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $this->load->view('base64encode/base64encodeinput');
    }

    public function enkrip(){
        $karakter = $this->input->post('karakter');
        $generate = base64_encode($karakter);

        if ($this->input->post('encrypt')) {     
            $data = array(
                'hasil' => $generate
            );
            $this->load->view('base64encode/base64encodehasil',$data);
        } else {
            $this->load->view('base64encode/base64encodeinput');
        }
    }
}
