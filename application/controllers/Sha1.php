<?php 

class Sha1 extends CI_Controller
{
    public function __construct(){
        parent::__construct();
    }
    
    public function index(){
        $this->load->view('sha1/sha1input');
    }

    public function enkrip(){
        $karakter = $this->input->post('karakter');
        $generate = sha1($karakter);

        if ($this->input->post('encrypt')) {     
            $data = array(
                'hasil' => $generate
            );
            $this->load->view('sha1/sha1hasil',$data);
        } else {
            $this->load->view('sha1/sha1input');
        }
    }
}
